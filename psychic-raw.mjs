/**
 * Psychic RAW
 *
 * Synchronizes:
 * - Thought to Verbal
 * - Emotion to Somatic
 *
 * Hides options not relevant to chosen spellcasting.
 */

const CFG = { module: 'pf1-psychic-raw' };

const traditionalToPsychic = {
	verbal: 'thought',
	somatic: 'emotion',
};

const psychicToTraditional = invertObject(traditionalToPsychic);

const allMappings = mergeObject(traditionalToPsychic, psychicToTraditional, { inplace: false });

const traditionalToPsychicKeys = {
	V: 'T',
	S: 'E',
};

const psychicToTraditionalKeys = invertObject(traditionalToPsychicKeys);

const base = 'data.components.';
const allComponentPaths = Object.keys(allMappings).reduce((o, c) => {
	const path = `${base}${c}`;
	o[c] = path;
	return o;
}, {});

// const keyMapping = mergeObject(traditionalToPsychicKeys, psychicToTraditionalKeys, { inplace: false });

function synchronizeComponents(components) {
	const updateData = { data: { components: {} } };

	let update = false;
	for (const [k, v] of Object.entries(components)) {
		const t = allMappings[k];
		if (t) {
			update = true;
			updateData.data.components[t] = v;
		}
	}

	return update ? updateData : null;
}

/**
 * Ensure new spells have the components correctly.
 *
 * @param {ItemSpellPF} spell
 * @param {Object} data Data used to create the instance.
 * @param {Object} options
 * @param {String} userId
 */
function fixComponentsOnCreate(spell, data, _options, _userId) {
	if (data.type !== 'spell') return;

	const components = data.components;
	if (!components) return;

	const updateData = synchronizeComponents(components)

	if (updateData) {
		// console.log('Correcting spell components:', updateData);
		spell.data.update(updateData);
	}
}

/**
 * Synchronize traditional magic and psychic magic components.
 *
 * @param {ItemSpellPF} spell
 * @param {Object} data
 * @param {Object} options
 * @param {String} userId
 */
function syncComponentsOnUpdate(spell, data, _options, _userId) {
	if (spell.type !== 'spell') return;

	const components = data.data?.components;
	if (components === undefined) return;

	const updateData = synchronizeComponents(components)

	if (updateData) {
		// console.log('Correcting spell components:', updateData);
		spell.data.update(updateData);
	}
}

/**
 * Adjust rendering on sheet.
 *
 * @param {ItemSheet} sheet
 * @param {JQuery} jq
 * @param {Object} options
 */
function fixSheetSpellComponents(sheet, jq, _options) {
	const item = sheet.document;
	if (item.type !== 'spell') return;

	const psychic = item.spellbook?.psychic;
	if (psychic === undefined) return; // Ignore items not on characters

	const traditionalKeys = Object.keys(traditionalToPsychic);
	const psychicKeys = Object.keys(psychicToTraditional);

	const html = jq[0];

	const components = item.data.data.components;

	for (const [c, path] of Object.entries(allComponentPaths)) {
		const cb = html.querySelector(`input[name="${path}"]`);
		if (cb) {
			const tk = psychic && traditionalKeys.includes(c),
				pk = !psychic && psychicKeys.includes(c),
				opposing = components[allMappings[c]];
			if (tk || pk) {
				// Hide opposing type components
				cb.readOnly = true;
				// cb.disabled = true;
				const parent = cb.parentElement;
				if (parent) parent.style.display = 'none'; // Hiding allows the components to be updated in the background
			}
			// console.log('Handling:', c, cb.checked, 'Opposing:', allMappings[c], opposing);
			if (opposing) cb.checked = opposing;
		}
	}
}

Hooks.on('preCreateItem', fixComponentsOnCreate);

Hooks.on('preUpdateItem', syncComponentsOnUpdate);

Hooks.on('renderItemSheetPF', fixSheetSpellComponents);

Hooks.on('init', () => {
	const getSpellComponents_original = CONFIG.Item.documentClasses.spell.prototype.getSpellComponents;

	CONFIG.Item.documentClasses.spell.prototype.getSpellComponents = function (srcData) {
		const components = getSpellComponents_original.call(this, srcData),
			psychic = this.spellbook?.psychic ?? false,
			mappings = psychic ? traditionalToPsychicKeys : psychicToTraditionalKeys;

		return components
			.map(c => mappings[c] ?? c)
			.filter((v, i, arr) => arr.indexOf(v) === i); // Remove duplicates
	}

	const mod = game.modules.get(CFG.module);
	console.log('PSYCHIC RAW |', mod.version ?? mod.data.version, '| READY!');
});
