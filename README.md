# Psychic RAW reinforcement for Pathfinder 1e

PF1 0.80.8 added support for psychic spellbooks. This makes it comply with RAW fully.

Fully synchronizes:

- Thought to Verbal
- Emotion to Somatic

Hides options not relevant to chosen spellcasting.

 ✅ Recommended for general use. Don't if you need homebrew support with components.

## Configuration

Not available.

## Install

Manifest URL: <https://gitlab.com/mkah-fvtt/pf1/psychic-raw/-/raw/latest/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
